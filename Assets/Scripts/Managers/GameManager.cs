﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static public GameManager Instance;

    [Header("Misc")]
    [SerializeField] UIMenuController MenuUpdater;
    [SerializeField] GameObject PlayerPrefab;

    [Header("Ingame objects")]
    [SerializeField] Camera GameCamera;
    [SerializeField] Camera DefaultCamera;

    GameObject PlayerGO;

    [SerializeField] GameObject PlayerGroup;
    [SerializeField] GameObject EnemyGroup;
    [SerializeField] GameObject EadiblesGroup;
    [SerializeField] GameObject GhostGroup;

    [Header("Phases")]
    [SerializeField] float PhaseTime;
    int GamePhase; //1: Logo+Player 2:Background 3: 4: 5:
    [SerializeField] GameObject TitleTextGO;
    [SerializeField] GameObject TileMaps;
    [SerializeField] GameObject ParticleSystem;
    [SerializeField] GameObject UserInterface;

    int PlayerPoints;

    public void ResetManager()
    {
        PlayerPoints = 0;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            this.ResetPhase();
            InvokeRepeating("IncrementPhase", PhaseTime, PhaseTime);

            this.SpawnPlayer();
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void IncrementPhase()
    {
        if (GamePhase > 4)
        {
            return;
        }

        GamePhase++;
        AudioManager.Instance.PlaySoundInteraction("PhaseChanged");

        if (GamePhase == 1)
        {
            AudioManager.Instance.PlaySoundInteraction("AddedUI");
            TitleTextGO.SetActive(true);
            UserInterface.SetActive(true);
            PlayerFeedbackManager.Instance.SetFeedbackText("Added: UI", 2);
        }
        if (GamePhase == 2)
        {
            AudioManager.Instance.PlaySoundInteraction("AddedMap");
            TileMaps.SetActive(true);
            PlayerFeedbackManager.Instance.SetFeedbackText("Added: Map", 2);
        }
        if (GamePhase == 3)
        {
            AudioManager.Instance.PlaySoundInteraction("AddedStars");
            ParticleSystem.SetActive(true);
            PlayerFeedbackManager.Instance.SetFeedbackText("Added: Stars", 2);
        }
    }

    public void ResetPhase()
    {
        GamePhase = 0;

        TitleTextGO.SetActive(false);
        TileMaps.SetActive(false);
        ParticleSystem.SetActive(false);
        UserInterface.SetActive(false);
    }

    public GameObject CurPlayerGO()
    {
        return PlayerGO;
    }

    public GameObject CurEnemyGroup()
    {
        return EnemyGroup;
    }

    public GameObject CurEadiblesGroup()
    {
        return EadiblesGroup;
    }

    public GameObject CurGhostGroup()
    {
        return GhostGroup;
    }

    public UIMenuController CurCanvasUpdater()
    {
        return MenuUpdater;
    }

    public void ResetLevel(bool _killPlayer)
    {
        AudioManager.Instance.StopPlaySoundInteraction("EnemyMoleculeMove");

        foreach (Transform curTransform in EadiblesGroup.transform)
        {
            Destroy(curTransform.gameObject);
        }

        foreach (Transform curTransform in EnemyGroup.transform)
        {
            Destroy(curTransform.gameObject);
        }

        foreach (Transform curTransform in GhostGroup.transform)
        {
            Destroy(curTransform.gameObject);
        }

        if (_killPlayer == true)
        {
            foreach (Transform curTransform in PlayerGroup.transform)
            {
                Debug.Log("Killed... " + GameManager.Instance.CurPlayerGO().name);
                Destroy(curTransform.gameObject);
            }
        }
    }

    public float DistanceToPlayerVolume(Vector2 _position)
    {
        float MaxDistance = 20f;
        float distance;

        if (PlayerGO != null)
            distance = Mathf.Clamp(Vector2.Distance(_position, PlayerGO.transform.position), 0f, MaxDistance);
        else
            distance = 5f;

        float volume;

        volume = Mathf.Clamp(((MaxDistance - distance) / MaxDistance), 0f, 1f);

        return volume;
    }

    public void ShowGameObjects(bool _doShow)
    {
        PlayerGO.SetActive(_doShow);
        EnemyGroup.SetActive(_doShow);
        EadiblesGroup.SetActive(_doShow);
        GhostGroup.SetActive(_doShow);

        if (_doShow == false)
        {
            DefaultCamera.gameObject.SetActive(true);
        }
    }

    public void ClearGhosts()
    {
        foreach (Transform curTransform in GhostGroup.transform)
        {
            Destroy(curTransform.gameObject);
        }
    }

    public void AdjustPoints(int _adjustment)
    {
        PlayerPoints += _adjustment;
    }

    public int CurPoints()
    {
        return PlayerPoints;
    }

    public void ResetManagers()
    {
        this.ResetLevel(true);
        GameManager.Instance.ResetManager();
        LevelManager.Instance.ResetManager();
        TimeManager.Instance.ResetManager();
        TutorialManager.Instance.ResetManager();

        this.SpawnPlayer();
    }

    public void SpawnPlayer()
    {
        GameObject SpawnedPlayer;

        if (GameManager.Instance.CurPlayerGO() != null)
            return;

        SpawnedPlayer = Instantiate(PlayerPrefab);
        SpawnedPlayer.transform.parent = PlayerGroup.transform;
        SpawnedPlayer.transform.position = Vector3.zero;

        PlayerGO = SpawnedPlayer;
    }
}
