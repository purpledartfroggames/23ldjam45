﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialManager : MonoBehaviour
{
    static public TutorialManager Instance;

    bool MovementTeached;
    bool StaminaTeached;
    bool FirstEadibleEaten;
    bool FirstEadibleToxicEaten;
    bool FirstEnemyEaten;
    bool GhostCloseEncounter;
    bool ObjectiveTold;

    bool GhostSpawned = false;

    [Header("Panel")]
    [SerializeField] GameObject TutorialPanel;

    [Header("Colors")]
    [SerializeField] Color colorDone;
    [SerializeField] Color colorRemain;

    [Header("Tutorials")]
    [SerializeField] TextMeshProUGUI MovementTutorial;
    [SerializeField] TextMeshProUGUI StaminaTutorial;
    [SerializeField] TextMeshProUGUI EadibleTutorial;
    [SerializeField] TextMeshProUGUI EadibleToxicTutorial;
    [SerializeField] TextMeshProUGUI EnemyTutorial;
    [SerializeField] TextMeshProUGUI GhostTutorial;
    [SerializeField] TextMeshProUGUI ObjectiveTutorial;

    public void ResetManager()
    {
        MovementTeached = false;
        StaminaTeached = false;
        FirstEadibleEaten = false;
        FirstEadibleToxicEaten = false;
        FirstEnemyEaten = false;
        GhostCloseEncounter = false;
        ObjectiveTold = false;
        GhostSpawned = false;
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (TutorialActive() == true)
        {
            if (TutorialFinished() == true)
            {
                Invoke("EndTutorial", 2f);
            }

            if (MovementTeached && StaminaTeached && FirstEadibleEaten && FirstEadibleToxicEaten && FirstEnemyEaten && !GhostCloseEncounter && !GhostSpawned)
            {
                GhostSpawned = true;
                GameObject.FindObjectOfType<EnemySpawner>().SpawnGhost();
            }
        }
    }

    private void EndTutorial()
    {
        Debug.LogWarning("End tutorial!");
        GameManager.Instance.ResetLevel(false);
        GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().SetTokens(5);
        LevelManager.Instance.LaunchNextLevel();
    }

    #region Status
    public bool TutorialActive()
    {
        bool IsTutorialActive;

        IsTutorialActive = LevelManager.Instance.GetCurLevel().IsTutorialLevel();

        return IsTutorialActive;
    }

    public bool TutorialFinished()
    {
        bool IsTutorialFinished;

        IsTutorialFinished = MovementTeached && StaminaTeached && FirstEadibleEaten && FirstEadibleToxicEaten && FirstEnemyEaten && GhostCloseEncounter && ObjectiveTold;

        return IsTutorialFinished;
    }
    #endregion

    public bool GetMovementTeached()        { return MovementTeached;  }
    public bool GetStaminaTeached()         { return StaminaTeached; }
    public bool GetFirstEadibleEaten()      { return FirstEadibleEaten; }
    public bool GetFirstEadibleToxicEaten() { return FirstEadibleToxicEaten; }
    public bool GetFirstEnemyEaten()        { return FirstEnemyEaten; }
    public bool GetGhostCloseEncounter()    { return GhostCloseEncounter; }
    public bool GetObjectiveTold()          { return ObjectiveTold; }

    public void SetMovementTeached(bool _tutorialTeached)        {  MovementTeached = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingMove");
                                                                    MovementTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Movement done!", 1);  }
    public void SetStaminaTeached(bool _tutorialTeached)         {  StaminaTeached = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingStamina");
                                                                    StaminaTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Stamina done!", 1); }
    public void SetFirstEadibleEaten(bool _tutorialTeached)      {  FirstEadibleEaten = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingEadible");
                                                                    EadibleTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Neutron done!", 1); }
    public void SetFirstEadibleToxicEaten(bool _tutorialTeached) {  FirstEadibleToxicEaten = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingToxic");
                                                                    EadibleToxicTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Corrupt atom done!", 1); }
    public void SetFirstEnemyEaten(bool _tutorialTeached)        {  FirstEnemyEaten = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingEnemy");
                                                                    EnemyTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Hostile molecule done!", 1); }
    public void SetGhostCloseEncounter(bool _tutorialTeached)    {  GhostCloseEncounter = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingAntimatter");
                                                                    GhostTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("Anti-matter done!", 1); }
    public void SetObjectiveTold(bool _tutorialTeached)          {  ObjectiveTold = _tutorialTeached;
                                                                    AudioManager.Instance.PlaySoundInteraction("TrainingComplete");
                                                                    ObjectiveTutorial.color = colorDone;
                                                                    PlayerFeedbackManager.Instance.SetFeedbackText("All tutorials done!", 1); }

    public void SetTutorialPanelActive(bool _active)
    {
       TutorialPanel.SetActive(_active && TutorialActive());
    }

}
