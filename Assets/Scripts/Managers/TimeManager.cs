﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    [SerializeField] bool GamePaused = true;

    public void ResetManager()
    {
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }

        //DontDestroyOnLoad(Instance);
    }

    public void SetPaused(bool _mustPause)
    {
        if (_mustPause == true)
        {
            GamePaused = true;
            Time.timeScale = 0f;
        }
        else
        {
            GamePaused = false;
            Time.timeScale = 1f;
        }
    }

    public bool IsPaused()
    {
        return GamePaused;
    }
}
