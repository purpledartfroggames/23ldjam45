﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerFeedbackManager : MonoBehaviour
{
    static public PlayerFeedbackManager Instance;

    [SerializeField] TextMeshProUGUI PlayerFeedbackText;
    [SerializeField] float ClearTime;
    float ClearBy;

    [Header("Colors")]
    [SerializeField] Color colorHigh;
    [SerializeField] Color colorMedium;
    [SerializeField] Color colorLow;
    [SerializeField] Color colorDefault;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (Time.time > ClearBy)
        {
            PlayerFeedbackText.text = "";
        }
    }

    public void SetFeedbackText(string _feedback, int _level1Low)
    {
        Color setColor = colorDefault;

        switch (_level1Low)
        {
            case 1:
                setColor = colorLow;
                break;
            case 2:
                setColor = colorMedium;
                break;
            case 3:
                setColor = colorHigh;
                break;

        }
        PlayerFeedbackText.color = setColor;
        PlayerFeedbackText.text = _feedback + "\n" + PlayerFeedbackText.text;
        PlayerFeedbackText.maxVisibleLines = 4;
        ClearBy = Time.time + ClearTime;
    }
}
