﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    static public LevelManager Instance;

    [SerializeField] Level[] Levels;

    float LevelChanged;

    int CurLevelIdx = 0;
    Level CurLevel;
    int NumOfLevels;

    public void ResetManager()
    {
        CurLevelIdx = 0;
        CurLevel = Levels[0];
   }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            CurLevel = Levels[0];
            LevelChanged = Time.time;
            NumOfLevels = Levels.Length - 1;
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (Time.time > LevelChanged + LevelManager.Instance.CurGhostTime())
        {
            GameObject.FindObjectOfType<EnemySpawner>().SpawnGhost();
        }
    }

    public void LaunchNextLevel()
    {
        CurLevelIdx++;

        if (CurLevelIdx + 1 > Levels.Length)
        {
            TimeManager.Instance.SetPaused(true);
            GameManager.Instance.CurCanvasUpdater().EndGame(true);
            AudioManager.Instance.PlaySoundInteractionVolumePitch("GameWon", 1f);
            return;
        }

        CurLevel = Levels[CurLevelIdx];

        LevelChanged = Time.time;

        GameManager.Instance.CurCanvasUpdater().UpdateInterface();
        AudioManager.Instance.PlaySoundInteractionVolumePitch("LevelChange", 1f);
        TutorialManager.Instance.SetTutorialPanelActive(CurLevel.IsTutorialLevel() == true);
        GameManager.Instance.SpawnPlayer();
    }

    public Level GetCurLevel()
    {
        return CurLevel;
    }

    public int GetCurLevelNum()
    {
        return CurLevelIdx + 1;
    }

    public int GetMaxLevel()
    {
        return Levels.Length;
    }

    public float CurGhostTime()
    {
        if (Levels.Length < CurLevelIdx + 1)
            return 99f;

        return Levels[CurLevelIdx].GetGhostTime();
    }

    public int GetNumOfLevels()
    {
        return NumOfLevels;
    }
}
