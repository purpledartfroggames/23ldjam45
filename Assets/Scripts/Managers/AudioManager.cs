﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    static public AudioManager Instance;

    public Sound[] soundsInteraction;
    public Sound[] soundsThemes;

    int musicId;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Invoke("StartMusic", 0f);
        }
        else
        {
            Destroy(this);
            return;
        }

        //DontDestroyOnLoad(this.gameObject);

        foreach (Sound s in soundsInteraction)
        {
            s.source = this.gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }

        foreach (Sound s in soundsThemes)
        {
            s.source = this.gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    public void ResetManager() //EVERY MANAGER!
    {

    }

    public void PlaySoundInteraction(string name)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            Debug.LogWarning("Sound request NOT found! " + name);
            return;
        }

        s.source.Play();
    }

    public void PlaySoundInteractionVolumePitch(string name, float volume)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        if (s.source.isPlaying == false)
        {
            s.source.pitch = UnityEngine.Random.Range(0.8f, 1.2f);
            s.source.volume = volume;
            s.source.Play();
        }
    }

    public void StopSoundInteraction(string name)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        s.source.pitch = 1f;
        s.source.volume = 1f;
        s.source.Stop();
    }

    public void StopPlaySoundInteraction(string name)
    {
        Sound s = Array.Find(soundsInteraction, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        if (s.source == null)
        {
            return;
        }

        if (s.source.isPlaying == true)
        {
            s.source.Stop();
        }
    }

    public void PlaySoundTheme(string name)
    {
        Sound s = Array.Find(soundsThemes, sound => sound.name == name);

        if (s == null)
        {
            return;
        }

        if (s.source == null)
        {
            return;
        }

        if (s.source.isPlaying == true)
        {
            s.source.Stop();
        }

        else if (true)
        {
            s.source.Play();
        }
    }

    public void StartMusic()
    {
        musicId = UnityEngine.Random.Range(0, 3);

        PlayerFeedbackManager.Instance.SetFeedbackText(soundsThemes[musicId].TrackTitle + " is playing!", 0);
        this.PlaySoundTheme(soundsThemes[musicId].name);
    }
    
    public void ToggleMusic()
    {
        this.PlaySoundTheme(soundsThemes[musicId].name);
    }

}
