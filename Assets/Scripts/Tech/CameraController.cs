﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Vector3 CameraOffset;
    Vector3 origin;

    // Start is called before the first frame update
    void Start()
    {
        CameraOffset = this.transform.position - GameManager.Instance.CurPlayerGO().transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CurPlayerGO() != null)
            origin = GameManager.Instance.CurPlayerGO().transform.position;

        this.transform.position = origin + CameraOffset;
    }
}
