﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridRealigner : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CurPlayerGO() == null)
            return;

        this.gameObject.transform.position = new Vector2((int)Mathf.Round(GameManager.Instance.CurPlayerGO().transform.position.x / 5f) * 5f, (int)Mathf.Round(GameManager.Instance.CurPlayerGO().transform.position.y / 5f) * 5f);
    }
}
