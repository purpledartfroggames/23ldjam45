﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            GameManager.Instance.CurCanvasUpdater().StopGame();
        }
        else if (Input.GetKeyUp(KeyCode.M))
        {
            AudioManager.Instance.ToggleMusic();
        }
        else if (Input.GetKeyUp(KeyCode.L))
        {
            GameManager.Instance.ResetLevel(false);
            GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().SetTokens(5);
            LevelManager.Instance.LaunchNextLevel();
            PlayerFeedbackManager.Instance.SetFeedbackText("Yoooou... cheat...", 3);
        }
        else if (Input.GetKeyUp(KeyCode.R))
        {
            GameManager.Instance.CurCanvasUpdater().RestartGame();
        }
    }
}
