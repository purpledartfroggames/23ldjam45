﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    Vector2 moveVector;

    [SerializeField] Rigidbody2D myRigibody;

    [SerializeField] float smoothTime;

    Vector2 MyVelocity = Vector2.zero;

    bool movedLeft, movedRight, movedUp, movedDown;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {
        moveVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        this.TutorialMove();
    }

    private void TutorialMove()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetMovementTeached() == false)
            {
                if (moveVector.x > 0f)
                    movedRight = true;
                else if (moveVector.x < 0f)
                    movedLeft = true;
                if (moveVector.y > 0f)
                    movedUp = true;
                else if (moveVector.y < 0f)
                    movedDown = true;

                if (movedRight && movedLeft && movedUp && movedDown)
                {
                    TutorialManager.Instance.SetMovementTeached(true);
                }
            }
        }
    }

    private void TutorialStamina()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetStaminaTeached() == false)
            {
                TutorialManager.Instance.SetStaminaTeached(true);
            }
        }
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift) && this.GetComponent<PlayerStamina>().StaminaAvailable())
        {
            this.TutorialStamina();
            moveVector = moveVector * 2f * this.GetComponent<PlayerStamina>().CurStamina();
        }


        myRigibody.MovePosition(Vector2.SmoothDamp(myRigibody.transform.position, (Vector2)myRigibody.transform.position + moveVector, ref MyVelocity, smoothTime));

        if (moveVector != Vector2.zero)
            AudioManager.Instance.PlaySoundInteractionVolumePitch("MoleculeMove", 1f);
        else if (moveVector == Vector2.zero)
            AudioManager.Instance.StopPlaySoundInteraction("MoleculeMove");
    }
}
