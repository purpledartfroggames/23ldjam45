﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStamina : MonoBehaviour
{
    float Stamina = 1f;

    // Update is called once per frame
    void Update()
    {
        this.AdjustStamina();
    }

    private void AdjustStamina()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            Stamina -= (Time.deltaTime / 10f);
        }
        else
        {
            Stamina += (Time.deltaTime / 10f);
        }

        Stamina = Mathf.Clamp(Stamina, 0f, 1f);
    }

    public bool StaminaAvailable()
    {
        return Stamina > 0f;
    }

    public float CurStamina()
    {
        return Stamina;
    }

    public void SetStamina(float _stamina)
    {
        Stamina = _stamina;
    }
}
