﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class UIMenuController : MonoBehaviour
{
    [Header("Groups")]
    [SerializeField] GameObject MainTitleGroup;
    [SerializeField] GameObject InGameGroup;
    [SerializeField] GameObject InGameBottomGroup;
    [SerializeField] GameObject EndGameGroup;
    [SerializeField] GameObject FeedbackPanel;

    [Header("Fields")]
    [SerializeField] TextMeshProUGUI TokenCount;
    [SerializeField] TextMeshProUGUI PointCount;
    [SerializeField] TextMeshProUGUI LevelText;

    [Header("Buttons")]
    [SerializeField] GameObject ButtonAboutHelp;
    [SerializeField] GameObject ButtonQuit;

    [Header("Bars")]
    [SerializeField] GameObject StaminaBar;

    [Header("GameOver")]
    [SerializeField] TextMeshProUGUI GameOverHeaderText;
    [SerializeField] TextMeshProUGUI GameOverDescriptionText;
    [SerializeField] TextMeshProUGUI GameOverRestartButtonText;

    int MaxTokens = 29;

    // Start is called before the first frame update
    void Start()
    {
        this.SwitchToMainTitlePanel();   
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.CurPlayerGO() == null)
            return; 

        TokenCount.text = GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().CurNumOfTokens().ToString("00") + "/" + MaxTokens.ToString("00");
        PointCount.text = GameManager.Instance.CurPoints().ToString();
        StaminaBar.transform.localScale = new Vector2(GameManager.Instance.CurPlayerGO().GetComponent<PlayerStamina>().CurStamina(), 1f);
    }

    private void DisableAllPanels()
    {
        MainTitleGroup.SetActive(false);
        InGameGroup.SetActive(false);
        InGameBottomGroup.SetActive(false);
        EndGameGroup.SetActive(false);
    }

    public void SwitchToMainTitlePanel()
    {
        this.DisableAllPanels();

        MainTitleGroup.SetActive(true);
    }

    public void SwitchToIngamePanel()
    {
        this.DisableAllPanels();

        InGameGroup.SetActive(true);
        InGameBottomGroup.SetActive(true);
    }

    public void UpdateInterface()
    {
        if (GameManager.Instance.CurPlayerGO() == null)
            return;

        LevelText.text = LevelManager.Instance.GetCurLevel().GetLevelNum().ToString("00") +"/" + LevelManager.Instance.GetNumOfLevels().ToString("00") + " : " + LevelManager.Instance.GetCurLevel().GetLevelTitle();
    }

    public void StartGame()
    {
        this.UnhideExtraButtons();
        this.ResumeGame();
    }

    public void StartSkipTutorialGame()
    {
        this.UnhideExtraButtons();
        this.ResumeGame();
        
        LevelManager.Instance.LaunchNextLevel();
        TutorialManager.Instance.SetTutorialPanelActive(false);
    }

    private void UnhideExtraButtons()
    {
        ButtonAboutHelp.SetActive(true);
        ButtonQuit.SetActive(true);
    }

    public void RestartGame()
    {
        AudioManager.Instance.PlaySoundInteraction("UIButtonClicked");
        FeedbackPanel.SetActive(true);

        TimeManager.Instance.SetPaused(true);
        GameManager.Instance.ResetManagers();

        SceneManager.LoadScene(0);
    }

    public void ResumeGame()
    {
        AudioManager.Instance.PlaySoundInteraction("UIButtonClicked");
        FeedbackPanel.SetActive(true);

        if (GameManager.Instance.CurPlayerGO() != null)
        {
            GameManager.Instance.CurPlayerGO().SetActive(true);
            TutorialManager.Instance.SetTutorialPanelActive(true);
        }

        this.SwitchToIngamePanel();
        GameManager.Instance.ShowGameObjects(true);
        TimeManager.Instance.SetPaused(false);
    }

    public void StopGame()
    {
        TimeManager.Instance.SetPaused(true);
        this.SwitchToMainTitlePanel();
        GameManager.Instance.ShowGameObjects(false);
    }

    public void EndGame(bool _won)
    {
        FeedbackPanel.SetActive(false);

        if (_won)
        {
            GameOverHeaderText.text = "Game won!";
            GameOverDescriptionText.text = "You succesfully completed the game.\nThanks for playing\n\nHave a great voting session on LD Jam 45";
            GameOverRestartButtonText.text = "Do it again!";
        }
        else
        {
            GameOverHeaderText.text = "Game lost!";
            GameOverDescriptionText.text = "You were eaten by the competition\nGo ahead - try your luck again.\n\nHave a great voting session on LD Jam 45";
            GameOverRestartButtonText.text = "Restart";
        }

        this.DisableAllPanels();
        EndGameGroup.SetActive(true);
    }

    public void QuitGame()
    {
        AudioManager.Instance.PlaySoundInteraction("UIButtonClicked");
        Application.Quit();
    }
}
