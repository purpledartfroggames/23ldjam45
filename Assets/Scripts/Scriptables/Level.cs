﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelXX", menuName = "Scriptables/Level")]
public class Level : ScriptableObject
{
    [SerializeField] string LevelTitle;
    [SerializeField] float EnemySpeedMod;

    [SerializeField] int LevelNum;
    [SerializeField] int MaxEnemies;
    [SerializeField] int MaxEadibles;

    [SerializeField] float GhostTime;

    [SerializeField] bool TutorialLevel;

    public string GetLevelTitle()
    {
        return LevelTitle;
    }

    public float GetEnemySpeedMod()
    {
        return EnemySpeedMod;
    }

    public int GetLevelNum()
    {
        return LevelNum;
    }

    public int GetMaxEnemies()
    {
        return MaxEnemies;
    }

    public int GetMaxEadibles()
    {
        return MaxEadibles;
    }

    public float GetGhostTime()
    {
        return GhostTime;
    }

    public bool IsTutorialLevel()
    {
        return TutorialLevel;
    }
}
