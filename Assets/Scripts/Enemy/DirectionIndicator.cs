﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionIndicator : MonoBehaviour
{
    [SerializeField] GameObject MyIndicator;
    [SerializeField] float DirectionIndicatorRange;

    GameObject MyPlayerGO;
    Transform MyPlayer;

    // Start is called before the first frame update
    void Start()
    {
        MyPlayerGO = GameObject.FindGameObjectWithTag("Player");

        if (MyPlayerGO != null)
        {
            MyPlayer = MyPlayerGO.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (MyPlayerGO != null && MyPlayerGO.GetInstanceID() == this.gameObject.GetInstanceID())
        {
            MyIndicator.SetActive(false);
            return;
        }

        if (MyPlayerGO == null)
        {
            MyPlayerGO = GameObject.FindGameObjectWithTag("Player");

        }
        if (MyPlayerGO == null)
        {
            MyIndicator.transform.position = this.transform.position;
        }
        else
        {
            Vector2 IndicatorPosition;

            MyPlayer = MyPlayerGO.transform;
            IndicatorPosition = this.transform.position - MyPlayer.position;

            if (Vector2.Distance(this.transform.position, MyPlayer.position) < DirectionIndicatorRange)
                MyIndicator.transform.position = (Vector2)MyPlayer.position + (IndicatorPosition.normalized * 4f);
            else
                MyIndicator.transform.position = this.transform.position;
        }
    }
}
