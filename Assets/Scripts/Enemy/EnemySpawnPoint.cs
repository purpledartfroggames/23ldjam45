﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnPoint : MonoBehaviour
{
    [SerializeField] [Range(10, 60)] float MinDistance;
    [SerializeField] [Range(2, 60)] float SpawnPointTo;

    Vector2 SpawnPoint;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void AdjustSpawn()
    {
        this.DetermineSpawnPoint();
        this.SetSpawnPoint();
    }

    private void DetermineSpawnPoint()
    {
        bool NegativeX = Random.Range(0, 2) == 1 ? true : false;
        bool NegativeY = Random.Range(0, 2) == 1 ? true : false;
        GameObject PlayerGO = GameManager.Instance.CurPlayerGO();


        do
        {
            float PosX = Random.Range(0f, SpawnPointTo);
            float PosY = Random.Range(0f, SpawnPointTo);

            if (NegativeX == true)
                PosX = PosX * -1;
            if (NegativeY == true)
                PosY = PosY * -1;

            SpawnPoint = new Vector2(PosX, PosY);
        }
        while (Vector2.Distance(GameManager.Instance.CurPlayerGO().transform.position, SpawnPoint) < MinDistance);
    }

    private void SetSpawnPoint()
    {
        GameObject PlayerGO = GameManager.Instance.CurPlayerGO();
        float distanceFloat = Vector2.Distance(PlayerGO.transform.position, SpawnPoint) * PlayerGO.GetComponent<TokenHolder>().CurNumOfTokens() / 30;
        this.transform.position = SpawnPoint;
    }
}
