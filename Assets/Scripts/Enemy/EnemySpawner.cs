﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] GameObject PrefabEnemy;
    [SerializeField] GameObject PrefabEadibles;
    [SerializeField] GameObject PrefabGhost;

    [SerializeField] GameObject EnemyGroup;
    [SerializeField] GameObject EadiblesGroup;
    [SerializeField] GameObject GhostGroup;

    [SerializeField] float SpawnRate;
    [SerializeField] int InitialBurst;

    int SpawnedEnemies;
    int SpawnedEadibles;
    int SpawnedGhosts;

    float LastSpawn;

    private void Start()
    {
        Invoke("DoBurst", 0.1f);
    }

    private void DoBurst()
    {
        for (int i = 0; i < InitialBurst; i++)
        {
            SpawnEnemy();
            SpawnEadible();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (TimeManager.Instance.IsPaused() == false)
        {
            if (GameManager.Instance.CurPlayerGO() != null
            && (Time.time > LastSpawn + SpawnRate
            || LastSpawn == 0f))
            {
                LastSpawn = Time.time;

                for (int i = 0; i < GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().CurNumOfTokens(); i++)
                {
                    SpawnEnemy();
                    SpawnEadible();
                    SpawnEadible(); //HACK!
                }
            }
        }
    }

    private void SpawnEnemy()
    {
        GameObject SpawnedEnemy;

        if (LevelManager.Instance.GetCurLevel().GetMaxEnemies() <= GameManager.Instance.CurEnemyGroup().transform.childCount)
            return;

        SpawnedEnemies++;

        if (SpawnedEnemies > 999999)
            SpawnedEnemies = 0;

        SpawnedEnemy = Instantiate(PrefabEnemy);
        SpawnedEnemy.name = PrefabEnemy.name + SpawnedEnemies.ToString("000000");
        SpawnedEnemy.transform.parent = EnemyGroup.transform;

        this.ElementSpawned(SpawnedEnemy);
    }

    public void SpawnGhost()
    {
        GameObject SpawnedGhost;

        if (GameManager.Instance.CurGhostGroup().transform.childCount != 0)
            return;

        SpawnedGhosts++;

        if (SpawnedGhosts > 999999)
            SpawnedGhosts = 0;

        SpawnedGhost = Instantiate(PrefabGhost);
        SpawnedGhost.name = PrefabGhost.name + SpawnedGhosts.ToString("000000");
        SpawnedGhost.transform.parent = GhostGroup.transform;

        AudioManager.Instance.PlaySoundInteractionVolumePitch("AntimatterTracked", 1f);
        PlayerFeedbackManager.Instance.SetFeedbackText("!!! Anti-matter detected !!!", 3);

        this.ElementSpawned(SpawnedGhost);
    }

    private void SpawnEadible()
    {
        GameObject SpawnedEadible;

        if (LevelManager.Instance.GetCurLevel().GetMaxEadibles() <= GameManager.Instance.CurEadiblesGroup().transform.childCount)
            return;

        SpawnedEadibles++;

        if (SpawnedEadibles > 999999)
            SpawnedEadibles = 0;

        SpawnedEadible = Instantiate(PrefabEadibles);
        SpawnedEadible.name = PrefabEadibles.name + SpawnedEadibles.ToString("000000");
        SpawnedEadible.transform.parent = EadiblesGroup.transform;

        this.ElementSpawned(SpawnedEadible);
    }

    public void AdjustSpawnRate(float _adjustment)
    {
        SpawnRate += _adjustment;
    }

    public void DEL_SpawnSingleGhost()
    {
        GameObject SpawnedGhost;

        SpawnedGhosts++;

        if (SpawnedGhosts > 999999)
            SpawnedGhosts = 0;

        SpawnedGhost = Instantiate(PrefabGhost);
        SpawnedGhost.name = PrefabGhost.name + SpawnedGhosts.ToString("000000");
        SpawnedGhost.transform.parent = GhostGroup.transform;

        AudioManager.Instance.PlaySoundInteractionVolumePitch("GhostArrived", 1f);
    }

    private void ElementSpawned(GameObject _spawnedElement)
    {
        if (GameManager.Instance.CurPlayerGO() == null)
            return;

        EnemySpawnPoint mySpawnPoint = _spawnedElement.GetComponent<EnemySpawnPoint>();

        if (mySpawnPoint == null)
            return;
        
        mySpawnPoint.AdjustSpawn();
    }
}
