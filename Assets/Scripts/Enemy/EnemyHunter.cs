﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHunter : MonoBehaviour
{
    GameObject PlayerGO;
    GameObject EnemyGroup;
    GameObject EadibleGroup;

    Rigidbody2D myRigibody;

    TokenHolder PlayerTokenHolder;

    [SerializeField] float smoothTime;
    [SerializeField] bool PlayerOnly;

    [Header("Indicator")]
    [SerializeField] SpriteRenderer DirectionIndicatorSprite;
    [SerializeField] Color colorGreen;
    [SerializeField] Color colorRed;

    [SerializeField] string MySoundString;


    Vector2 MyVelocity = Vector2.zero;

    GameObject CurTarget;

    // Start is called before the first frame update
    void Start()
    {
        PlayerGO = GameManager.Instance.CurPlayerGO();
        EnemyGroup = GameManager.Instance.CurEnemyGroup();
        EadibleGroup = GameManager.Instance.CurEadiblesGroup();

        myRigibody = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (PlayerGO != null)
        {
            this.Hunt();
        }
    }

    private GameObject _FindTarget()
    {
        GameObject target = null;
        PlayerTokenHolder = PlayerGO.GetComponent<TokenHolder>();

        if (PlayerTokenHolder == null)
            return null;

        if (PlayerTokenHolder.CurNumOfTokens() < this.GetComponent<TokenHolder>().CurNumOfTokens())
            return PlayerGO;

        foreach (Transform curTransform in EnemyGroup.transform)
        {
            if (curTransform.gameObject.GetComponent<TokenHolder>().CurNumOfTokens() < this.GetComponent<TokenHolder>().CurNumOfTokens())
            {
                return curTransform.gameObject;
            }
        }

        float NearestDist = 0f;
        GameObject NearestGO = null;

        foreach (Transform curTransform in EadibleGroup.transform)
        {
            float CurDist = Vector2.Distance(transform.position, curTransform.position);

            if (NearestDist == 0f || CurDist < NearestDist)
            {
                NearestDist = CurDist;
                NearestGO = curTransform.gameObject;
            }

        }

        return NearestGO;
    }

    private GameObject FindTarget()
    {
        GameObject NearestGO = null;

        PlayerTokenHolder = PlayerGO.GetComponent<TokenHolder>();

        if (PlayerTokenHolder == null)
            return null;

        if (PlayerOnly == true || (PlayerTokenHolder.CurNumOfTokens() < this.GetComponent<TokenHolder>().CurNumOfTokens()))
        {
            return PlayerGO;
        }

        NearestGO = this.FindNearest(EnemyGroup, true);

        if (NearestGO == null)
            NearestGO = this.FindNearest(EadibleGroup, false);

        return NearestGO;
    }

    private GameObject FindNearest(GameObject _group, bool _tokenDependent)
    {
        float NearestDist = 0f;
        GameObject NearestGO = null;

        foreach (Transform curTransform in EadibleGroup.transform)
        {
            float CurDist = Vector2.Distance(transform.position, curTransform.position);

            if (_tokenDependent == false
            || curTransform.gameObject.GetComponent<TokenHolder>().CurNumOfTokens() < this.GetComponent<TokenHolder>().CurNumOfTokens())
            {
                if (NearestDist == 0f || CurDist < NearestDist)
                {
                    NearestDist = CurDist;
                    NearestGO = curTransform.gameObject;
                }
            }

        }

        return NearestGO;
    }

    private void Hunt()
    {
        Vector2 moveVector;

        if (CurTarget == null
        || CurTarget.GetComponent<TokenHolder>().CurNumOfTokens() > this.GetComponent<TokenHolder>().CurNumOfTokens())
        {
            CurTarget = this.FindTarget();
        }

        if (CurTarget == null)
            return;

        if (DirectionIndicatorSprite != null)
        {
            if (GameManager.Instance.CurPlayerGO() != null && CurTarget.GetInstanceID() == GameManager.Instance.CurPlayerGO().GetInstanceID())
                DirectionIndicatorSprite.color = colorRed;
            else
                DirectionIndicatorSprite.color = colorGreen;
        }

        moveVector = CurTarget.transform.position - this.transform.position;
        moveVector = moveVector.normalized;
        moveVector = this.TutorialMoveSpeed(moveVector);
        
        myRigibody.MovePosition(Vector2.SmoothDamp(myRigibody.transform.position, (Vector2)myRigibody.transform.position + moveVector, ref MyVelocity, smoothTime * LevelManager.Instance.GetCurLevel().GetEnemySpeedMod()));

        if (moveVector != Vector2.zero)
            AudioManager.Instance.PlaySoundInteractionVolumePitch(MySoundString, GameManager.Instance.DistanceToPlayerVolume(this.transform.position));
        else if (moveVector == Vector2.zero)
            AudioManager.Instance.StopPlaySoundInteraction(MySoundString);
    }

    private Vector3 TutorialMoveSpeed(Vector3 _origMoveVector)
    {
        float MoveVectorModifier = 1f;

        if (TutorialManager.Instance.TutorialActive() == true && this.gameObject.GetComponent<Ghost>() != null)
            MoveVectorModifier = 2f;

        return _origMoveVector * MoveVectorModifier;
    }
}
