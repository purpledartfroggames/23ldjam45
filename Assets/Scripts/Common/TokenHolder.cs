﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenHolder : MonoBehaviour
{
    [SerializeField] int NumOfTokens;

    [SerializeField] GameObject[] TokenIndicator;
    [SerializeField] float AccumulateTime;
    [SerializeField] CircleCollider2D MyCollider;

    float LastAccumulation;

    // Update is called once per frame
    void Start()
    {
        if (this.GetComponent<Enemy>() == null)
        {
            this.AdjustTokens(0); //HACK!
        }
        else
        {
            this.AdjustTokens(Random.Range(-1, 2));
        }
    }

    private void Update()
    {
        if (this.tag == "Player"
        && NumOfTokens >= TokenIndicator.Length)
        {
            if (TutorialManager.Instance.TutorialActive() == true)
            {
                this.TutorialObjective();
            }
            else
            {
                GameManager.Instance.ResetLevel(false);
                GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().SetTokens(5);
                LevelManager.Instance.LaunchNextLevel();
            }
        }

        if (TutorialManager.Instance.TutorialActive() == true
        &&  TutorialManager.Instance.TutorialFinished() == true)
        {
            GameManager.Instance.ResetLevel(false);
            GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>().SetTokens(5);
            LevelManager.Instance.LaunchNextLevel();
        }

        this.AccumulateToken();
    }

    private void TutorialObjective()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetObjectiveTold() == false)
            {
                TutorialManager.Instance.SetObjectiveTold(true);
            }
        }
    }

    private void AccumulateToken()
    {
        if (AccumulateTime != 0f
        && NumOfTokens < TokenIndicator.Length)
        {
            if (Time.time >= LastAccumulation + (AccumulateTime * this.CurTier()))
            {
                LastAccumulation = Time.time;
                this.AdjustTokens(1);
            }
        }

    }

    public void AdjustTokens(int _adjustment)
    {
        this.AdjustPoints(_adjustment);

        NumOfTokens = Mathf.Clamp(NumOfTokens + _adjustment, 1, TokenIndicator.Length);

        this.ResetIndicators();
        this.ResetCollider();
    }

    private void AdjustPoints(int _tokens)
    {
        int PointAdjustment = 0;

        if (_tokens < 0)
        {
            PointAdjustment = _tokens * -200;
        }
        else
        {
            PointAdjustment = _tokens * 100;
        }

        GameManager.Instance.AdjustPoints(PointAdjustment);
    }

    public void SetTokens(int _tokens)
    {
        NumOfTokens = Mathf.Clamp(_tokens, 1, TokenIndicator.Length);

        this.ResetIndicators();
    }

    private void ResetIndicators()
    {
        for (int i = 0; i < TokenIndicator.Length; i++)
        {
            TokenIndicator[i].SetActive(NumOfTokens > i);
            GameManager.Instance.CurCanvasUpdater().UpdateInterface();
        }
    }

    public int CurNumOfTokens()
    {
        return NumOfTokens;
    }

    public int CurTier()
    {
        if (NumOfTokens > 13)
            return 4;
        else if (NumOfTokens > 5)
            return 3;
        else if (NumOfTokens > 1)
            return 2;
        else
            return 1;
    }

    private void ResetCollider()
    {
        float ColliderRadius = 1f;

        if (MyCollider == null)
            return;

        if (NumOfTokens > 13)
            ColliderRadius = 2.5f;
        else if (NumOfTokens > 5)
            ColliderRadius = 2f;
        else if (NumOfTokens > 1)
            ColliderRadius = 1.5f;
        else
            ColliderRadius = 1f;

        MyCollider.radius = ColliderRadius;
    }
}
