﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// BLOATED AS ****

public class CircleCollision : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        #region Variables
        GameObject MyGO = this.gameObject;
        GameObject ColliderGO = collision.gameObject;

        TokenHolder MyGOTokenHolder;
        TokenHolder ColliderGOTokenHolder;

        if (MyGO == null
        || ColliderGO == null)
        {
            return;
        }

        if (MyGO.tag == "Player"
        || ColliderGO.tag == "Player")
        {
            CameraShake.Shake(.2f, .2f);
        }

        MyGOTokenHolder = MyGO.GetComponent<TokenHolder>();
        ColliderGOTokenHolder = ColliderGO.GetComponent<TokenHolder>();

        if (MyGOTokenHolder == null
        || ColliderGOTokenHolder == null)
        {
            return;
        }
        #endregion

        if (MyGOTokenHolder.CurNumOfTokens() < ColliderGOTokenHolder.CurNumOfTokens()
        && MyGOTokenHolder.gameObject.tag != "Eadible"
        && ColliderGOTokenHolder.gameObject.tag != "Eadible")
        {
            if((MyGO.GetComponent<Ghost>() != null
            && ColliderGOTokenHolder.gameObject.tag == "Player")
            || (ColliderGOTokenHolder.gameObject.GetComponent<Ghost>() != null
            && MyGO.tag == "Player"))
            {
                this.TutorialEatGhost();  
                return;
            }
            if (ColliderGOTokenHolder.tag == "Player")
            {
                this.TutorialEatEnemy();
            }
            this.EatAndDestroy(ColliderGOTokenHolder.gameObject, MyGOTokenHolder.gameObject, "TokenPickup", false);
        }
        else if (MyGO.tag == "Eadible" && MyGO.GetComponent<ToxicEadible>().GetIsToxic() == true)
        {
            if (ColliderGO.tag == "Player")
                this.TutorialEatEadibleToxic();

            this.EatenToxic(ColliderGOTokenHolder.gameObject, MyGOTokenHolder.gameObject, "EadibleToxic");
        }
        else if (MyGO.tag == "Eadible")
        {
            bool EadibleIrrelevant = ColliderGOTokenHolder.CurTier() >= 4;
            string PickupSound = EadibleIrrelevant == false ? "TokenPickup" : "TokenPickupNull";
            this.TutorialEatEadible();
            this.EatAndDestroy(ColliderGOTokenHolder.gameObject, MyGOTokenHolder.gameObject, PickupSound, EadibleIrrelevant);

        }
    }

    private void TutorialEatEadible()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetFirstEadibleEaten() == false)
            {
                TutorialManager.Instance.SetFirstEadibleEaten(true);
            }
        }
    }

    private void TutorialEatEadibleToxic()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetFirstEadibleToxicEaten() == false)
            {
                TutorialManager.Instance.SetFirstEadibleToxicEaten(true);
            }
        }
    }


    private void TutorialEatEnemy()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetFirstEnemyEaten() == false)
            {
                TutorialManager.Instance.SetFirstEnemyEaten(true);
            }
        }
    }

    private void TutorialEatGhost()
    {
        if (TutorialManager.Instance.TutorialActive() == true)
        {
            if (TutorialManager.Instance.GetGhostCloseEncounter() == false)
            {
                TutorialManager.Instance.SetGhostCloseEncounter(true);
                GameManager.Instance.ClearGhosts();

                TokenHolder CurTokenHolder = GameManager.Instance.CurPlayerGO().GetComponent<TokenHolder>();

                CurTokenHolder.AdjustTokens(28 - CurTokenHolder.CurNumOfTokens());
            }
        }
    }

    private void EatAndDestroy(GameObject _eater, GameObject _eaten, string _sound, bool _irrelevant)
    {
        AudioManager.Instance.PlaySoundInteractionVolumePitch(_sound, GameManager.Instance.DistanceToPlayerVolume(_eaten.transform.position));

        if (_irrelevant == false)
            _eater.GetComponent<TokenHolder>().AdjustTokens(1);

        this.GameOverScreen(_eaten);

        if (_eaten.tag == "Enemy")
            AudioManager.Instance.PlaySoundInteractionVolumePitch("EnemyEaten", 1f);
        else if (_eaten.tag == "Eadible")
            AudioManager.Instance.PlaySoundInteractionVolumePitch("EadibleEaten", 1f);

        Destroy(_eaten);
    }

    private void EatenToxic(GameObject _eater, GameObject _eaten, string _sound)
    {
        AudioManager.Instance.PlaySoundInteractionVolumePitch(_sound, GameManager.Instance.DistanceToPlayerVolume(_eaten.transform.position));

        _eater.GetComponent<TokenHolder>().AdjustTokens(-_eater.GetComponent<TokenHolder>().CurNumOfTokens() / 2);

        this.GameOverScreen(_eaten); //Should be neutron

        Destroy(_eaten);
    }

    private void GameOverScreen(GameObject _eaten)
    {
        if (_eaten.tag == "Player")
        {
            AudioManager.Instance.PlaySoundInteractionVolumePitch("GameOver", 1f);
            TimeManager.Instance.SetPaused(true);
            GameManager.Instance.CurCanvasUpdater().EndGame(false);
        }
    }

}
