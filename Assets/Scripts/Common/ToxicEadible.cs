﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToxicEadible : MonoBehaviour
{
    bool IsToxic;

    [SerializeField] SpriteRenderer[] PossibleToxicPart;
    [SerializeField] Color ToxicColor;

    SpriteRenderer ChosenPart;

    // Start is called before the first frame update
    void Start()
    {
        IsToxic = Random.Range(0, 40) == 1;

        if (TutorialManager.Instance.TutorialActive())
        {
            IsToxic = IsToxic || this.TutorialSpawnToxic();
        }

        if (IsToxic == true)
        {
            ChosenPart = PossibleToxicPart[Random.Range(0, Mathf.Min(PossibleToxicPart.Length, this.GetComponent<TokenHolder>().CurNumOfTokens()))];
            ChosenPart.color = ToxicColor;

            if (IsToxic == true)
            {
                Invoke("KillFlower", 20f);
            }
        }
    }

    private bool TutorialSpawnToxic()
    {
        bool SpawnToxic = Random.Range(0, 8) == 1;

        return SpawnToxic;
    }

    private void KillFlower()
    {
        Destroy(this.gameObject);
    }

    public bool GetIsToxic()
    {
        return IsToxic;
    }
}
